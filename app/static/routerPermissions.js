'use strict';

module.exports = [
  { route: "/api/user/login", roles: 0 },
  { route: "/api/common/uploadFile", roles: [1, 2, 3] }
];
