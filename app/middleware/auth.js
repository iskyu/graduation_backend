'use strict';
// const permissions = require('../static/routerPermissions.js');

module.exports = roles => {
  return async (ctx, next) => {
    const token = ctx.request.header.token;
    const checkAuthRes = await ctx.service.authorize.checkAuth(token);
    if (checkAuthRes.errcode) {
      ctx.status = 401;
      ctx.body = {
        errcode: 1,
        msg: '授权失败，请重新登录'
      };
      return false;
    }
    const identity = checkAuthRes.data.identity;
    if (roles && roles.indexOf(identity) === -1) {
      ctx.status = 403;
      ctx.body = {
        errcode: 1,
        msg: '您没有权限！'
      };
      return false;
    }

    await next();
  };
};
