'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  const auth = app.middleware.auth;
  router.get('/', controller.home.index);

  /* 学生 */
  // 新增学生
  router.post('/api/student/insert', auth([1]), controller.student.insertStudent);
  // 根据学号查找学生
  router.get('/api/student/findByNum', auth([1]), controller.student.findStudentByNum);
  // 根据_id更新学生信息
  router.post('/api/student/updateOneById', auth([1]), controller.student.updateOneStudentById);
  // 根据_id删除一个学生
  router.post('/api/student/removeOneById', auth([1]), controller.student.removeOneById);
  // 批量添加学生
  router.post('/api/student/insertByXlsx', auth([1]), controller.student.insertByXlsx);
  // 查询学生
  router.post('/api/student/search', auth([1]), controller.student.searchStudent);
  // 批量删除学生
  router.post('/api/student/removeMany', auth([1]), controller.student.removeManyStudentById);
  // 给一个学生分配宿舍
  router.post('/api/student/assignDorm', auth([1]), controller.student.assignsDorm);
  // 批量分配宿舍
  router.post('/api/student/bulkAssignDorm', auth([1]), controller.student.bulkAssignDorm);
  // 学生退寝
  router.post('/api/student/setLeave', auth([1]), controller.student.setLeave);
  // 批量学生退寝
  router.post('/api/student/bulkSetLeave', auth([1]), controller.student.bulkSetLeave);

  // 学生提交退寝申请表
  router.post('/api/student/postLeaveForm', auth([2]), controller.student.postLeaveForm);
  // 学生查看已提交的退寝申请单
  router.post('/api/student/studentSubmittedLeaveForm', auth([2]), controller.student.studentSubmittedLeaveForm);
  // 管理员查询左右的学生退寝申请表
  router.post('/api/student/searchLeaveForm', auth([1]), controller.student.searchLeaveForm);
  // 管理员同意退寝
  router.post('/api/student/agreeLeave', auth([1]), controller.student.agreeLeave);
  // 管理员拒绝退寝
  router.post('/api/student/rejectLeave', auth([1]), controller.student.rejectLeave);
  // 管理员删除退寝申请单
  router.post('/api/student/removeLeave', auth([1]), controller.student.removeLeave);

  // 学生提交调整宿舍申请表
  router.post('/api/student/postAdjustForm', auth([2]), controller.student.postAdjustForm);
  // 学生查看已提交的调整宿舍申请单
  router.post('/api/student/studentSubmittedAdjustForm', auth([2]), controller.student.studentSubmittedAdjustForm);
  // 管理员查询左右的学生调整宿舍申请表
  router.post('/api/student/searchAdjustForm', auth([1]), controller.student.searchAdjustForm);
  // 管理员同意调整宿舍
  router.post('/api/student/agreeAdjust', auth([1]), controller.student.agreeAdjust);
  // 管理员拒绝调整宿舍
  router.post('/api/student/rejectAdjust', auth([1]), controller.student.rejectAdjust);
  // 管理员删除调整宿舍申请单
  router.post('/api/student/removeAdjust', auth([1]), controller.student.removeAdjust);

  // 填写离校登记表
  router.post('/api/student/editLeaveSchoolForm', auth([2]), controller.student.editLeaveSchoolForm);
  // 删除离校登记表
  router.post('/api/student/delLeaveSchoolForm', auth([1]), controller.student.delLeaveSchoolForm);
  // 查询离校登记表
  router.post('/api/student/searchLeaveSchoolForm', auth([1]), controller.student.searchLeaveSchoolForm);
  // 学生查看自己的离校登记表
  router.post('/api/student/findSelfLeaveSchoolForm', auth([2]), controller.student.findSelfLeaveSchoolForm);


  /* 楼栋 */
  // 批量新增楼栋
  router.post('/api/building/insertMany', auth([1]), controller.building.insertManyBuildings);
  // 更新楼栋
  router.post('/api/building/update', auth([1]), controller.building.updateBuilding);
  // 根据楼栋号查找楼栋
  router.get('/api/building/findByNum', auth([1]), controller.building.findBuildingByNum);
  // 查找所有楼栋
  router.get('/api/building/findAll', auth([1]), controller.building.findAllBuilding);
  // 查询楼栋
  router.post('/api/building/search', auth([1]), controller.building.searchBuildings);
  // 删除一个楼栋
  router.post('/api/building/removeOne', auth([1]), controller.building.removeOneBuilding);
  // 修改楼栋信息
  router.post('/api/building/updateBuilding', auth([1]), controller.building.updateBuilding);

  /* 宿舍 */
  // 批量新增宿舍
  router.post('/api/dorm/insertMany', auth([1]), controller.dorm.insertManyDorm);
  // 根据宿舍号查找宿舍
  router.get('/api/dorm/findByNum', auth([1]), controller.dorm.findDormByNumber);
  // 查询宿舍
  router.post('/api/dorm/search', auth([1]), controller.dorm.searchDorms);
  // 根据宿舍号查询宿舍人数
  router.post('/api/dorm/checkStudentCount', auth(), controller.dorm.checkDormStudentCount);
  // 删除宿舍byId
  router.post('/api/dorm/removeOne', auth([1]), controller.dorm.removeOneDorm);
  // 修改宿舍信息
  router.post('/api/dorm/updateDorm', auth([1]), controller.dorm.updateDorm);

  // 上传文件
  router.post('/api/common/uploadFile', auth(), controller.common.uploadFile);
  // 上传文件到缓存文件夹
  router.post('/api/common/uploadFileToCache', auth(), controller.common.uploadFileToCache);
  // 删除静态文件
  router.post('/api/common/delStaticFile', auth(), controller.common.delStaticFile);
  // 获取首页信息
  router.post('/api/common/getHomeInfo', auth(), controller.common.getHomeInfo);

  // 登录
  router.post('/api/user/login', controller.user.login);
  // 获取当前登录用户信息
  router.post('/api/user/getUserInfo', auth(), controller.user.getUserInfo);
  // 修改密码
  router.post('/api/user/updatePassword', auth(), controller.user.updatePassword);

  // 添加管理员
  router.post('/api/admin/insertOne', auth([1]), controller.admin.insertOneAdmin);
  // 查询管理员
  router.post('/api/admin/searchAdmin', auth([1]), controller.admin.searchAdmin);

  // 添加维修员
  router.post('/api/repairmen/insertOne', auth([1]), controller.repairmen.insertOneRepairmen);
  // 查询维修员
  router.post('/api/repairmen/searchRepairmen', auth([1]), controller.repairmen.searchRepairmen);
  // 修改维修员
  router.post('/api/repairmen/updateRepairmen', auth([1]), controller.repairmen.updateRepairmen);
  // 删除维修员
  router.post('/api/repairmen/delRepairmen', auth([1]), controller.repairmen.delRepairmen);

  // 学生填写设备保修单
  router.post('/api/repair/editRepairForm', auth([2]), controller.repair.editRepairForm);
  // 学生查看已提交的设备报修单
  router.post('/api/repair/studentSubmittedRepairForm', auth([2]), controller.repair.studentSubmittedRepairForm);

  // 查询设备报修单
  router.post('/api/repair/searchRepairForm', auth([1, 3]), controller.repair.searchRepairForm);
  // 开始处理维修单
  router.post('/api/repair/startHandlerRepairForm', auth([3]), controller.repair.startHandlerRepairForm);
  // 已完成处理维修单
  router.post('/api/repair/repairFormDone', auth([3]), controller.repair.repairFormDone);
  // 维修单另约时间
  router.post('/api/repair/makeRepairFormAnotherTime', auth([3]), controller.repair.makeRepairFormAnotherTime);
  // 删除维修单
  router.post('/api/repair/delRepairForm', auth([3]), controller.repair.delRepairForm);
};
