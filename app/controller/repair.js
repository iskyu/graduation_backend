"use strict";

const Controller = require("egg").Controller;

class RepairController extends Controller {
  //  学生填写设备保修单
  async editRepairForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const token = ctx.request.headers.token;
    const id = await ctx.service.authorize.getUserId(token);
    const payload = ctx.request.body;
    payload.id = id;
    const res = await ctx.service.repair.editRepairForm(payload);

    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }

    ctx.body = resMsg;
  }

  // 学生查看已提交的设备报修单
  async studentSubmittedRepairForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const token = ctx.request.headers.token;
    const id = await ctx.service.authorize.getUserId(token);
    const res = await ctx.service.repair.studentSubmittedRepairForm(id);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 查询设备报修单
  async searchRepairForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const repairmen = await ctx.service.authorize.getUserId(ctx.request.headers.token);
    const res = await ctx.service.repair.searchRepairForm(payload, repairmen);
    resMsg.data = res;
    ctx.body = resMsg;
  }

  // 开始处理维修单
  async startHandlerRepairForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const repairmen = await ctx.service.authorize.getUserId(ctx.request.headers.token);
    const res = await ctx.service.repair.startHandlerRepairForm(payload.id, repairmen);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 已完成处理维修单
  async repairFormDone() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const repairmenId = await ctx.service.authorize.getUserId(ctx.request.headers.token);
    const res = await ctx.service.repair.repairFormDone(payload.id, repairmenId);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 维修单另约时间
  async makeRepairFormAnotherTime() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const repairmenId = await ctx.service.authorize.getUserId(ctx.request.headers.token);
    const res = await ctx.service.repair.makeRepairFormAnotherTime(payload.id, repairmenId);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 删除维修单
  async delRepairForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.repair.delRepairForm(payload.id);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }
}

module.exports = RepairController;
