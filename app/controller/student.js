"use strict";

const Controller = require("egg").Controller;
class StudentController extends Controller {
  /**
   * @api {POST} /student/insert 添加学生
   * @apiParam {Object} Student 学生信息
   */
  async insertStudent() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;

    if (await ctx.service.student.findStudentByNum(payload.student_num)) {
      resMsg.msg = "学号已存在！";
      resMsg.errcode = 1;
      ctx.body = resMsg;
      return false;
    }
    const res = await ctx.service.student.insertOneStudent(payload);
    if (res.errcode) {
      resMsg.msg = res.msg;
      resMsg.errcode = res.errcode;
    }

    ctx.body = resMsg;
  }

  /**
   * @desc 根据学号查找学生
   * @apiParam {Number} student_num 学号
   */
  async findStudentByNum() {
    const { ctx } = this;
    const resMsg = {
      errcode: 0,
      data: {},
      msg: "success"
    };
    const student_num = ctx.request.query.student_num;
    resMsg.data = await ctx.service.student.findStudentByNum(student_num);
    ctx.body = resMsg;
  }

  // 更新一名学生的信息 (根据_id)
  async updateOneStudentById() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const data = ctx.request.body;

    const isExist = await ctx.service.student.findOneById(data._id);
    if (!isExist) {
      resMsg.errcode = 1;
      resMsg.msg = "该学生不存在";
      ctx.body = resMsg;
      return false;
    }

    await ctx.service.student.updateOneStudentById(data);
    ctx.body = resMsg;
  }

  // 根据Id删除一个学生的信息
  async removeOneById() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    // resMsg.test = ctx.service.student.findOneById(ctx.request.body._id);
    if (!ctx.request.body._id) {
      resMsg.errcode = 1;
      resMsg.msg = "缺少 _id";
    } else {
      const res = await ctx.service.student.removeOneById(ctx.request.body._id);
      if (res.errcode) {
        resMsg.errcode = res.errcode;
        resMsg.msg = res.msg;
      }
    }
    ctx.body = resMsg;
  }

  // 通过xlsx批量导入学生
  async insertByXlsx() {
    const ctx = this.ctx;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;

    try {
      const errorList = await ctx.service.student.insertByXlsx(payload.fileUrl);
      if (errorList) {
        resMsg.errcode = 1;
        resMsg.msg = "数据格式有误，请核对。";
        resMsg.data = errorList;
      }
    } catch (e) {
      console.error(e);
      resMsg.errcode = -1;
      resMsg.msg = e;
    }

    ctx.body = resMsg;
  }

  // 查询学生
  async searchStudent() {
    const ctx = this.ctx;
    const resMsg = ctx.helper.resMsg();
    const conditions = ctx.request.body;
    const res = await ctx.service.student.searchStudent(conditions);
    resMsg.data = res;
    ctx.body = resMsg;
  }

  // 批量删除学生
  async removeManyStudentById() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;

    const res = await this.service.student.removeManyStudentById(payload.students);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = "部分学生删除失败";
      resMsg.errorStudents = res.errorStudents;
    }

    ctx.body = resMsg;
  }

  // 给一个学生分配宿舍
  async assignsDorm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.student.assignsDorm(payload);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    }

    ctx.body = resMsg;
  }

  // 批量分配宿舍
  async bulkAssignDorm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.student.bulkAssignDorm(payload);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    }
    ctx.body = resMsg;
  }

  // 办理退寝
  async setLeave() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.student.setLeave(payload.id);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    }
    ctx.body = resMsg;
  }

  // 批量办理退寝
  async bulkSetLeave() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.student.bulkSetLeave(payload);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
      resMsg.errorStudents = res.errorStudents;
    }
    ctx.body = resMsg;
  }

  // 学生提交退寝申请单
  async postLeaveForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const token = ctx.request.headers.token;
    payload.id = await ctx.service.authorize.getUserId(token);
    const res = await ctx.service.student.postLeaveForm(payload);

    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    }
    ctx.body = resMsg;
  }

  // 学生查看已提交的退寝申请单
  async studentSubmittedLeaveForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const token = ctx.request.headers.token;
    const id = await ctx.service.authorize.getUserId(token);
    const res = await ctx.service.student.studentSubmittedLeaveForm(id);

    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }

    ctx.body = resMsg;
  }

  // 管理员查询左右的学生退寝申请表
  async searchLeaveForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.student.searchLeaveForm(payload);
    resMsg.data = res;
    ctx.body = resMsg;
  }

  // 同意退寝
  async agreeLeave() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const approver = await ctx.service.authorize.getUserId(ctx.request.headers.token);
    const res = await ctx.service.student.agreeLeave(payload.id, approver);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 拒绝退寝
  async rejectLeave() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const approver = await ctx.service.authorize.getUserId(ctx.request.headers.token);
    const res = await ctx.service.student.rejectLeave(payload.id, approver);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }
  // 删除退寝申请单
  async removeLeave() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.student.removeLeave(payload.id);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }


  // 学生提交调整宿舍申请单
  async postAdjustForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const token = ctx.request.headers.token;
    payload.id = await ctx.service.authorize.getUserId(token);
    const res = await ctx.service.student.postAdjustForm(payload);

    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    }
    ctx.body = resMsg;
  }

  // 学生查看已提交的调整宿舍申请单
  async studentSubmittedAdjustForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const token = ctx.request.headers.token;
    const id = await ctx.service.authorize.getUserId(token);
    const res = await ctx.service.student.studentSubmittedAdjustForm(id);

    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }

    ctx.body = resMsg;
  }

  // 管理员查询左右的学生调整宿舍申请表
  async searchAdjustForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.student.searchAdjustForm(payload);
    resMsg.data = res;
    ctx.body = resMsg;
  }

  // 同意调整宿舍
  async agreeAdjust() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const approver = await ctx.service.authorize.getUserId(ctx.request.headers.token);
    const res = await ctx.service.student.agreeAdjust(payload.id, approver);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 拒绝调整宿舍
  async rejectAdjust() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const approver = await ctx.service.authorize.getUserId(ctx.request.headers.token);
    const res = await ctx.service.student.rejectAdjust(payload.id, approver);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }
  // 删除调整宿舍申请单
  async removeAdjust() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.student.removeAdjust(payload.id);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 填写离校登记表
  async editLeaveSchoolForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const token = ctx.request.headers.token;
    payload.id = await ctx.service.authorize.getUserId(token);
    const res = await ctx.service.student.editLeaveSchoolForm(payload);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 删除离校登记表
  async delLeaveSchoolForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;

    const res = await ctx.service.student.delLeaveSchoolForm(payload.id);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 查询离校登记表
  async searchLeaveSchoolForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.student.searchLeaveSchoolForm(payload);
    resMsg.data = res;
    ctx.body = resMsg;
  }

  // 学生查看自己的离校登记表
  async findSelfLeaveSchoolForm() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const token = ctx.request.headers.token;
    const id = await ctx.service.authorize.getUserId(token);
    const res = await ctx.service.student.findSelfLeaveSchoolForm(id);

    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }

    ctx.body = resMsg;
  }

}

module.exports = StudentController;
