'use strict';

const Controller = require('egg').Controller;

class CommonController extends Controller {
  // 上传文件，返回地址给前端
  async uploadFile() {
    const { ctx } = this;
    const path = await ctx.service.common.uploadFile();
    const resMsg = ctx.helper.resMsg();
    resMsg.data = path;
    ctx.body = resMsg;
  }
  async uploadFileToCache() {
    const { ctx } = this;
    const path = await ctx.service.common.uploadFile('cache/');
    const resMsg = ctx.helper.resMsg();
    resMsg.data = path;
    ctx.body = resMsg;
  }

  // 删除静态资源文件
  async delStaticFile() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const staticPath = ctx.request.body.url;
    const res = await ctx.service.common.delStaticFile(staticPath);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // // 批量删除静态资源文件
  // async bulkDelStaticFile() {
  //   const { ctx } = this;
  //   const resMsg = ctx.helper.resMsg();
  //   const fileList = ctx.request.body.fileList;
  //   const res = await ctx.service.common.bulkDelStaticFile(fileList);
  //   if (res.errcode) {
  //     resMsg.errcode = res.errcode;
  //     resMsg.msg = res.msg;
  //   }
  //   ctx.body = resMsg;
  // }

  // 首页信息
  async getHomeInfo() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const res = await ctx.service.common.getHomeInfo();
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }
}

module.exports = CommonController;
