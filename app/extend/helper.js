"use strict";
const path = require('path');

class ResMsg {
  constructor() {
    this.errcode = 0;
    this.data = {};
    this.msg = "success";
  }
}

module.exports = {
  // 生成regMsg
  resMsg() {
    return new ResMsg();
  },

  // 过滤掉一个对象内为undefined的属性
  filterUndefined(data) {
    for (const key in data) {
      if (data[key] === undefined) {
        delete data[key];
      }
    }
    return data;
  },

  // 把静态资源文件路径转为绝对路径
  convertStaticToAbsolutePath(staticPath) {
    return path.join(this.config.baseDir, 'app/', staticPath);
  },

  // 返回分页信息
  getPaging(total, pageSize) {
    return {
      total,
      pageSize,
      pageCount: Math.ceil(total / pageSize)
    };
  }

};
