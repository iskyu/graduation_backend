'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const AuthorizeSchema = new Schema({
    // token
    token: {
      type: String,
      required: true,
      unique: true,
    },
    // 用户_id
    userId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    // 用户身份
    identity: {
      type: Number,
      required: true,
      /**
       * 1：管理员
       * 2：学生
       * 3：维修人员
       */
    },
    // 过期时间
    expire_time: {
      type: Number,
      required: true
    }
  });

  return mongoose.model('Authorize', AuthorizeSchema);
};
