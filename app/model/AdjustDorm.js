'use strict';
// 调整宿舍申请表
module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const AdjustDormSchema = new Schema({
    // 学生id
    student_id: {
      type: Schema.Types.ObjectId,
      ref: 'Student',
      required: true,
    },
    // // 原宿舍
    // old_dorm: {
    //     type: String, // 楼栋号#寝室号 如：8#604
    //     required: true,
    //     validate: {
    //         validator(value) {
    //             return /^\d+#\d+$/.test(value);
    //         },
    //         message: '{VALUE}不是合法的宿舍号',
    //     },
    // },
    // 目标宿舍
    target_dorm: {
      type: String, // 楼栋号#寝室号 如：8#604
      required: true,
      validate: {
        validator(value) {
          return /^\d+#\d+$/.test(value);
        },
        message: '{VALUE}不是合法的宿舍号',
      },
    },
    // 理由原因
    reason: {
      type: String,
      required: true,
    },
    edit_date: {
      type: Number,
      required: true,
    },
    // 审批时间
    approval_date: {
      type: Number, // 时间戳
    },
    // 审批人id
    approver: {
      type: Schema.Types.ObjectId,
      ref: 'Admin',
    },
    // 审批状态
    approval_status: {
      type: Number,
      default: 0,
      required: true,
      /**
       * 0 待审批
       * 1 已通过
       * 2 已驳回
       */
    }
  });

  return mongoose.model('AdjustDorm', AdjustDormSchema);
};

