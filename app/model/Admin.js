'use strict';
// 管理员表
module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const AdminSchema = new Schema({
        // 管理员账号
        admin_account: {
            type: String,
            required: true,
            unique: true
        },
        // 管理员密码
        admin_password: {
            type: String,
            required: true,
        },
        // 管理员姓名
        admin_name: {
            type: String,
            required: true,
        },
        // 添加时间
        add_time: {
            type: Number, // 时间戳
            required: true,
        },
        // 状态
        admin_status: {
            type: Number,
            /**
             * 0 不可用
             * 1 可用
             */
            required: true,
        },
        // 身份
        identity: {
            type: Number,
            required: true,
            default: 1, // 管理员的身份是1
        },

    });

    return mongoose.model('Admin', AdminSchema);
};
