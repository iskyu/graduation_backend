'use strict';
// 维修员表
module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const RepairmenSchema = new Schema({
        // 维修人员账号
        repairmen_account: {
            type: String,
            required: true,
        },
        // 维修人员密码
        repairmen_password: {
            type: String,
            required: true,
        },
        // 维修人员姓名
        repairmen_name: {
            type: String,
            required: true,
        },
        // 添加时间
        add_time: {
            type: Number, // 时间戳
            required: true,
        },
        // 账号状态
        repairmen_status: {
            type: Number,
            required: true,
            default: 1,
            /**
             * 0 不可用
             * 1 可用
             */
        },
        // 身份
        identity: {
            type: Number,
            required: true,
            default: 3, // 维修人的身份是3
        },
    });

    return mongoose.model('Repairmen', RepairmenSchema);
};
