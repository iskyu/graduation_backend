'use strict';
// 楼栋表
module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const BuildingSchema = new Schema({
    // 楼栋号
    building_num: {
      type: Number,
      unique: true,
    },
    // 楼层数
    floor: {
      type: Number,
      required: true,
    },
    // 描述
    desc: {
      type: String,
    }
  });
  return mongoose.model('Building', BuildingSchema);
};
