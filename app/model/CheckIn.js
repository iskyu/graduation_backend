'use strict';
// 入住表
module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const CheckInSchema = new Schema({
        // 学生id
        student_id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'Student',
        },
        // 入住时间
        checkin_date: {
            type: Number, // 时间戳
            required: true,
        },
        // 寝室id
        dorm_id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'Dorm',
        },
    });

    return mongoose.model('CheckIn', CheckInSchema);
};
