'use strict';
// 设备报修表
module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const Repair = new Schema({
    // // 报修人身份
    // initiator_indentity: {
    //     type: Number,
    //     required: true,
    //     /**
    //      * 1 管理员
    //      * 2 学生
    //      * 3 报修人员
    //      */
    // },
    // 报修人ID
    initiator_id: {
      type: Schema.Types.ObjectId,
      ref: 'Student',
      required: true,
    },
    // 报修人姓名
    // initiator_name: {
    //     type: String,
    //     required: true,
    // },
    // 宿舍Id
    dorm_id: {
      type: Schema.Types.ObjectId,
      ref: 'Dorm',
      required: true,
    },
    // 联系方式
    contact: {
      type: String,
      required: true,
    },
    // 维修描述
    repair_desc: {
      type: String,
      required: true,
    },
    // 图片
    repair_files: {
      type: [],
    },
    // 维修状态
    repair_status: {
      type: Number,
      default: -1,
      /**
       * -1 待处理
       * 1 正在处理
       * 2 已处理
       * 3 另约时间
       */
    },
    // 提交时间
    edit_date: {
      type: Number,
      required: true,
    },
    // 维修人ID
    repairmen_id: {
      type: Schema.Types.ObjectId,
      ref: 'Repairmen',
    }
  });

  return mongoose.model('Repair', Repair);
};
