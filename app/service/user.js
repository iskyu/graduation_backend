'use strict';

const Service = require('egg').Service;

class UserService extends Service {
  // 用户登录
  async login() {
    const { ctx } = this;
    const payload = ctx.request.body;
    const identity = +payload.identity;
    let account, userId;

    if (identity === 1) {
      // 管理员
      const authRes = await ctx.service.admin.authAdmin(payload.account, payload.password);
      if (authRes.errcode) {
        return authRes;
      }
      account = authRes.data.admin_account;
      userId = authRes.data._id;

    } else if (identity === 2) {
      // 学生
      const authRes = await ctx.service.student.authStudent(payload.account, payload.password);
      if (authRes.errcode) {
        return authRes;
      }
      account = authRes.data.student_num;
      userId = authRes.data._id;
    } else if (identity === 3) {
      // 维修员
      const authRes = await ctx.service.repairmen.authRepairmen(payload.account, payload.password);
      if (authRes.errcode) {
        return authRes;
      }
      account = authRes.data.repairmen_account;
      userId = authRes.data._id;
    }

    // 生成token
    const token = await ctx.service.authorize.getToken(account, identity);
    await ctx.service.authorize.insertOneAuth(token, userId, identity);

    return {
      errcode: 0,
      msg: 'success',
      data: token
    };
  }

  // 获取当前登录用户信息
  async getUserInfo(token) {
    const { ctx } = this;
    const auth = await ctx.service.authorize.checkAuth(token);
    if (auth.errcode) {
      return auth;
    }

    const identity = auth.data.identity;
    const userId = auth.data.userId;
    let userInfo;

    if (identity === 1) {
      const res = await ctx.service.admin.findAdminById(userId);
      userInfo = {
        account: res.admin_account,
        name: res.admin_name,
        identity: res.identity,
        avatar: this.config.ipAddress + "/public/images/admin.png",
        identity_name: "管理员",
        dorm_count: await ctx.model.Dorm.find().count(),
        student_count: await ctx.model.Student.find().count(),
        userId: res._id
      };
    } else if (identity === 2) {
      const res = await ctx.service.student.findOneById(userId);
      userInfo = {
        account: res.student_num,
        name: res.student_name,
        identity: res.identity,
        dorm_num: res.dorm_id && res.dorm_id.dorm_num,
        reside_status: res.reside_status,
        avatar: this.config.ipAddress + "/public/images/student.png",
        identity_name: "学生",
        userId: res._id
      };
    } else if (identity === 3) {
      const res = await ctx.model.Repairmen.findById(userId);
      userInfo = {
        account: res.repairmen_account,
        name: res.repairmen_name,
        identity: res.identity,
        avatar: this.config.ipAddress + "/public/images/repair.png",
        identity_name: "维修员",
        ongoing_task: await ctx.model.Repair.find({ repairmen_id: res._id, repair_status: { $ne: 2 } }).count(),
        userId: res._id
      };
    }
    return {
      errcode: 0,
      msg: "success",
      data: userInfo
    };
  }

  // 修改密码
  async updatePassword(data) {
    const { ctx } = this;
    const identity = data.identity;
    const userId = data.userId;

    if (identity === 1) {
      const admin = await ctx.model.Admin.findById(userId);
      if (admin.admin_password !== data.oldPassword) {
        return { errcode: 1, msg: "原密码错误！" };
      }
      admin.admin_password = data.newPassword;
      await admin.save();
    } else if (identity === 2) {
      const student = await ctx.model.Student.findById(userId);
      if (student.student_password !== data.oldPassword) {
        return { errcode: 1, msg: "原密码错误！" };
      }
      student.student_password = data.newPassword;
      await student.save();
    } else if (identity === 3) {
      const repairmen = await ctx.model.Repairmen.findById(userId);
      if (repairmen.repairmen_password !== data.oldPassword) {
        return { errcode: 1, msg: "原密码错误！" };
      }
      repairmen.repairmen_password = data.newPassword;
      await repairmen.save();
    }

    return { errcode: 0 };
  }

}

module.exports = UserService;
