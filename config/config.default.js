'use strict';

module.exports = appInfo => {
  const env = process.env.NODE_ENV;
  return {
    keys: appInfo.name + '_1565275322854_9248',
    cluster: {
      listen: {
        port: 7416,
      }
    },
    ipAddress: env === 'production' ? "http://dodopo.cn/dorm-after" : "http://127.0.0.1:7416",
    // middleware: ['auth'],
    security: {
      csrf: {
        enable: false,
      },
    },
    cors: {
      origin: '*',
      allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS'
    },
    multipart: {
      fileExtensions: ['.xlsx'],
    },
    mongoose: {
      client: {
        url: env === 'production' ? 'mongodb://USER:PWD@127.0.0.1:PORT/dormitory?authSource=DB' : "mongodb://127.0.0.1:27017/dormitory",
        options: {
          useFindAndModify: false,
        }
      }
    },
    auth_time: 7200000 // 登录过期时间（ms毫秒）
  };
};
